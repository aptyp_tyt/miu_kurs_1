#include "stdafx.h"
#include "data.h"
#include <Windows.h>
#include <string.h>


#define cls() system("cls");
#pragma warning (disable : 4996) // заглушка для MS Visual Studio 2017

/*
*
*   Взято из stackowerflow
*   fopen is deprecated
*	https://stackoverflow.com/questions/14386/fopen-deprecated-warning
*
*/

using namespace std;

bool is_file_saved = true;		// сохранен ли файл
int trip_count = -1;				// фактическое количество рейсов
const int max_trips_count = 100;
const int trip_size_bytes = sizeof(Trip);  // размер структуры в байтах
const char* data_file = "data.dat";  // имя файла с данными структур

FILE *f;
Trip trips[max_trips_count];

int program_exit();
void main_menu();
int get_console_int(); // объявляем прототип функции
void get_console_str(char str[], int max_size); // объявляем прототип функции
BTime get_console_time();
bool add_trip();
bool edit_trip(int id);
bool delete_trip(int id);
bool load_from_file(const char* file_name);
bool save_file(const char* file_name);
const char* trim_all_spaces(const char str[]);
const char* to_lower(const char str[]);

const char* to_lower(const char str[]) {
	const int sz = (int)(strlen(str));
	static char* buff = new char(sz);
	for (int i = 0; i < sz; i++) {
		buff[i] = tolower(str[i]);
	}
	buff[sz] = '\0';
	return buff;
}


const char* trim_all_spaces(const char str[]) {
	const int sz = (int)(strlen(str));
	static char* buff = new char(sz);
	int count = 0;
	for (int i = 0; i < sz; i++) {
		if ((str[i] != 32) & (str[i] != 9)) {
			buff[count] = str[i];
			count++;
		}
	}
	buff[count] = '\0';
	return buff;
}

struct Menus {
	int main_menu() {
		cls();
		cout << endl;
		cout << " 1 - Добавить рейс(ы)" << endl;
		cout << " 2 - Загрузить из файла" << endl;
		cout << " 3 - Отсортировать рейсы" << endl;
		cout << " 4 - Поиск рейса" << endl;
		cout << " 5 - Изменить рейс" << endl;
		cout << " 6 - Удалить рейс" << endl;
		cout << " 7 - Вывести все рейсы" << endl;
		cout << " 8 - Сохранить рейсы в файл" << endl;
		cout << " 9 - Просмотреть рейс" << endl;
		cout << " 0 - Выход" << endl;
		cout << endl;
		cout << left << " Выбор: ";
		int cmd = get_console_int();
		return cmd;
	}

	bool ask_yes_no(const char* message, const char* error = "") {
		if (error != "") {
			cls();
			cout << " " << error << endl;
		}

		cout << " " << message;

		int cmd = get_console_int();
		return (cmd == 1 || cmd == 2) ? cmd == 1 : ask_yes_no(message, "Неверная команда.");
	}

	int trip_show_additional_menu() {
		cout << endl << " ---------------------- " << endl << endl;
		cout << " 1 - Изменить\t 2 - Удалить\t 3 - Предыдущий рейс \t 4 - Следующий рейс \t 0 - Главное меню" << endl << " Ваш выбор: ";
		return get_console_int();
	}

	Trip ask_trip_data() {
		cls();
		Trip trip;
		cout << " Номер рейса: ";
		trip.trip_number = get_console_int();
		cout << endl;
		cout << " Тип автобуса: ";
		get_console_str(trip.bus_type, sizeof(trip.bus_type));
		cout << endl;
		cout << " Пункт назначения: ";
		get_console_str(trip.destination, sizeof(trip.destination));
		cout << endl;

		cout << " Время отправления (Формат HH:MM): ";
		trip.start_time = get_console_time();
		cout << endl;

		cout << " Время прибытия (Формат HH:MM): ";
		trip.dest_time = get_console_time();
		cout << endl;

		return trip;
	}

	int ask_trip_id(const char* message = "") {
		if (message != "") {
			cls();
			cout << " " << message << endl;
		}
		int cmd = 0;
		if (trip_count > 1) {
			cout << " ID рейса (от 0 до " << trip_count - 1 << " ): ";
			cmd = get_console_int();
		}

		return (cmd >= 0 || cmd < max_trips_count) ? cmd : ask_trip_id(" ОШИБКА! ID неверный!");
	}

	bool trips_not_exists() {
		if (trip_count == -1) {
			cls();
			cout <<" ОШИБКА!"<< endl << " Рейсов не существует. Создайте или загрузите." << endl;
			cout << " -------------------------" << endl << endl;
			system("pause");
		}
		return trip_count == -1;
	}

	Trip ask_search_trip_data() {
		cls();
		cout << " Поиск рейса " << endl << endl;
		Trip trip;
		cout << " Пункт назначения: ";
		get_console_str(trip.destination, sizeof(trip.destination));
		cout << endl;

		cout << " Желаемое время прибытия (Формат HH:MM): ";
		trip.dest_time = get_console_time();
		cout << endl;
		return trip;
	}

} menu;



int get_console_int() {
	char a[5], c[1000];
	int j = 0;
	cin.getline(c, 1000);
	for (int i = 0; i < 5; i++)
	{
		if (c[i] >= '0'&&c[i] <= '9')
		{
			a[j] = c[i]; j++;
		}
	}
	if (j == 0) return -1;

	return atoi(a);
}

void get_console_str(char str[], int max_size = 254) {
	max_size = (max_size > 254) ? 254 : max_size;
	char* a = new char[max_size+1];
	char c[1000];
	cin.getline(c, 1000);
	for (int i = 0; i < max_size; i++) {
		a[i] = c[i];
		if (c[i] == '\0')
			strcpy(str, a);
	}
	a[max_size] = '\0';
	strcpy(str, a);
}

BTime get_console_time() {
	BTime time = BTime(0,0);
	char chTime[2][3];
	char c[1000];
	int j = 0, t = 0, h, m;
	cin.getline(c, 1000);
	for (int i = 0; i < 1000; i++)
	{
		if (c[i] == ':' || c[i] == '.') // нашли разделитель - переходим к след. набору
		{t++; j=0;}

		if (c[i] >= '0' && c[i] <= '9')
		{
			if (j > 1){t++; j=0;} // если первый набор окончен - переходим к следующему
			if (t > 1) { break; } // если второй набор окончен - прерываем цикл
			chTime[t][j] = c[i];
			j++;
		}

	}
	h = atoi(chTime[0]);
	m = atoi(chTime[1]);
	if (h > 24 || h < 0) h = 0;
	if (m > 59 || m < 0) m = 0;

	if (t==0 && j == 0) return BTime(0, 0);

	return BTime(h, m);
}

void add_trips() {
	trip_count = (trip_count == -1) ? 0 : trip_count;
	is_file_saved = false;
	while (1) {
		if (!add_trip())
			break;
		if (!menu.ask_yes_no(" Добавить еще один рейс? (1 - Да, 2 - Нет): "))
			break;
	}
}

bool add_trip() {
	if (trip_count < max_trips_count) {
		Trip trip = menu.ask_trip_data();
		trips[trip_count] = trip;
		trip_count++;
		return true;
	}

	return false;
}

bool show_trip(int id = -1, Trip* trps = trips) {
	id = (id < 0) ? menu.ask_trip_id() : id;
	if (id < max_trips_count) {
		cout << " ID рейса: " << id << endl;
		cout << " Номер рейса: " << trps[id].trip_number << endl;
		cout << " Тип автобуса: " << trps[id].bus_type << endl;
		cout << " Пункт назначения: " << trps[id].destination << endl;
		cout << " Время отправления: " << trps[id].start_time.tostr() << endl;
		cout << " Время прибытия: " << trps[id].dest_time.tostr() << endl;
		cout << endl;
		return true;
	}
	return false;
}

bool show_trip_with_menu(int id = -1) {
	cls();
	id = (id < 0) ? menu.ask_trip_id() : id;
	if (show_trip(id)){
		switch (menu.trip_show_additional_menu())
		{
		case 1: 
			is_file_saved = (edit_trip(id)) ? false : is_file_saved;
			break;
		case 2:
			is_file_saved = (delete_trip(id)) ? false : is_file_saved;
			break;
		case 3:
			id = (id > 0) ? id-1 : id;
			show_trip_with_menu(id);
			break;
		case 4:
			id = (id < max_trips_count && id < trip_count-1) ? id+1 : id;
			show_trip_with_menu(id);
			break;

		case 0:
			main_menu();
			break;

		default:
			show_trip_with_menu(id);
			break;
		}

	}
	return false;
}

void show_all_trips() {
	cls();
	for (int i = 0; i < trip_count; i++) {
		if (!show_trip(i))
			break;
		else
			cout << "--------------" << endl << endl;
	}
	cout << endl;
	system("pause");
}

bool delete_trip(int id = -1) {
	id = (id < 0) ? menu.ask_trip_id() : id;
	if (id != trip_count - 1) {
		for (int i = id; i < trip_count; i++) {
			trips[i] = trips[i + 1];
		}
	}
	trip_count--;
	return true;
}

bool edit_trip(int id = -1) {
	id = (id < 0) ? menu.ask_trip_id() : id;
	if (!(id < max_trips_count))
		return false;

	bool changed = false;

	Trip old_trip_data = trips[id];
	trips[id] = menu.ask_trip_data();

	changed = (old_trip_data.trip_number != trips[id].trip_number) || (old_trip_data.bus_type != trips[id].bus_type) || (old_trip_data.destination != trips[id].destination);
	return changed;
}

bool file_exists(const char* file_name) {
	if (FILE *file = fopen(file_name, "r")) {
		fclose(file);
		return true;
	}
	return false;
}

bool load_from_file(const char* file_name) {
	
	if (!file_exists(file_name))
		if (!menu.ask_yes_no("Файла не существует. Желаете создать? 1 - Да, 2 - Нет: "))
			return false;

	f = fopen(data_file, "rb");

	Trip trip;
	int count = 0;
	fseek(f, 0, SEEK_SET);
	while (fread(&trip, 1, trip_size_bytes, f) == trip_size_bytes) {
		trips[count] = trip;
		count++;
	}
	fclose(f);
	trip_count = (count == 0) ? -1 : count;
	return true;
}

bool save_file(const char* file_name) {
	if (!file_exists(data_file))
		if (!menu.ask_yes_no("Файла не существует. Желаете создать? 1 - Да, 2 - Нет: "))
			return false;

	f = fopen(file_name, "wb+");

	fseek(f, 0, SEEK_SET);
	for (int i = 0; i < trip_count; i++) {
		fwrite(&trips[i], trip_size_bytes, 1, f);
	}
	fclose(f);
	return true;
}

void sort_trips_by_number(Trip* trips) {
	for (int arr_flag = 1; arr_flag < trip_count; arr_flag++)
		for (int curr_elem = arr_flag; curr_elem > 0; curr_elem--)
			if (trips[curr_elem - 1].trip_number > trips[curr_elem].trip_number)
				swap(trips[curr_elem - 1], trips[curr_elem]);
			else
				break;
}

void sort_trips_by_time(Trip* trips) {
	for (int arr_flag = 1; arr_flag < trip_count; arr_flag++)
		for (int curr_elem = arr_flag; curr_elem > 0; curr_elem--)
			if (trips[curr_elem - 1].dest_time > trips[curr_elem].dest_time)
				swap(trips[curr_elem - 1], trips[curr_elem]);
			else
				break;
}


void search_trip(Trip search_trip_data) {
	Trip* sorted_trips = trips;
	sort_trips_by_time(sorted_trips);
	int count = 0;
	cls();
	for (int i = 0; i < trip_count; i++) {
		char str1[255];
		char str2[255];
		strcpy(str1, trim_all_spaces(to_lower((search_trip_data.destination))));
		strcpy(str2, trim_all_spaces(to_lower((trips[i].destination))));
		

		if (strcmp(str1, str2) == 0) {
			if (trips[i].dest_time <= search_trip_data.dest_time) {
				count++;
				show_trip(i, sorted_trips);
			}
		}
	}
	cout << "-----------------"  << endl;
	if (count == 0)
		cout << "Подходящих рейсов не найдено" << endl << endl;
	else
		cout << "Найдено рейсов: " << count << " из " << trip_count << endl << endl;
	system("pause");
}


void main_menu() {
	while (1) {
		switch (int cmd = menu.main_menu()) {
		case 0:
			program_exit();
			break;
		case 1:
			add_trips();
			break;
		case 2:
			is_file_saved = load_from_file(data_file) || is_file_saved;
			break;
		case 3:
			if (menu.trips_not_exists())
				break;
			sort_trips_by_number(trips);
			is_file_saved = false;
			break;
		case 4:
			if (menu.trips_not_exists())
				break;
			search_trip(menu.ask_search_trip_data());
			break;
		case 5:
			if (menu.trips_not_exists())
				break;
			is_file_saved = (edit_trip()) ? false : is_file_saved;
			break;
		case 6:
			if (menu.trips_not_exists())
				break;
			is_file_saved = (delete_trip()) ? false : is_file_saved;
			break;
		case 7:
			if (menu.trips_not_exists())
				break;
			show_all_trips();
			break;
		case 8:
			if (menu.trips_not_exists())
				break;
			is_file_saved = save_file(data_file) || is_file_saved;
			break;
		case 9:
			if (menu.trips_not_exists())
				break;
			show_trip_with_menu();
			break;
		default:
			cout << "  '" << cmd << "' - Нет такой команды!" << endl << endl;
			system("pause");
			break;
		}
	}
}


void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	main_menu();
}

int program_exit() {
	if (!is_file_saved) {
		if (menu.ask_yes_no("Файл не сохранен. Желаете сохранить? 1-Да 2-Нет: "))
			is_file_saved = save_file(data_file) || is_file_saved;
	}
	exit(0);
}