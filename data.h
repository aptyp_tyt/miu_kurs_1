#pragma warning (disable : 4996)

struct BTime {
	short hour;
	short minutes;

	BTime(short h = 0, short m = 0)
	{
		hour = h; minutes = m;
	}

	const char* tostr() {
		static char s[6];
		sprintf(s, "%2.2d:%2.2d", hour, minutes);
		return s;
	}
};

struct Trip {
	int trip_number;
	char bus_type[51];  // +1 ������ ��� '\0'
	char destination[151]; // +1 ������ ��� '\0'
	BTime start_time, dest_time;
};

bool operator==(BTime &a, BTime &b) {
	return (a.hour == b.hour && a.minutes == b.minutes) ? 1 : 0;
}

bool operator!=(BTime &a, BTime &b) {
	return !(a == b);
}

bool operator>(BTime &a, BTime &b) {
	return ((a.hour == b.hour && a.minutes > b.minutes) || a.hour > b.hour) ? 1 : 0;
}

bool operator<(BTime &a, BTime &b) {
	return ((a.hour == b.hour && a.minutes < b.minutes) || a.hour < b.hour) ? 1 : 0;
}

bool operator>=(BTime &a, BTime &b) {
	return (a==b || a > b) ? 1 : 0;
}

bool operator<=(BTime &a, BTime &b) {
	return (a == b || a < b) ? 1 : 0;
}